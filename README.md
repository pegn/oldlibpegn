# PEGN Parser and AST Reference Implementation in Golang.

***Warning: we are still working toward a 1.0 release. Please do not use
for anything but experimentation until then.***

## TODO

* Make sure to `p.NewLine()` on `EndLine` parsing from AST.

* Drop json package dependency in node.UnmarshalJSON.

* Add verbose error messages at every level in the AST parsers.

* Auto-generated test cases from the `README.md` example code.

* Add the AST JSON format explicitly to the `grammar.pegn`

* Add the AST Pretty JSON format explicitly to the `grammar.pegn`

## Design Considerations

* This is a reference implementation with priority on easy to
  understand, sustainable, clear code and modular architecture over
  hyper-optimized performance. More performant parsers will certainly be
  created eventually, but those reference parsing need only be faster
  than regular expressions, but not as fast as parsers required for
  thousands of documents a second, more like for a language syntax
  server (PLS) or static site generator that has to work once every
  second or so even if it doesn't take that long to run.

* Since the JSON AST is expected to be used by applications to
  communicate and combine things every effort has been made to use the
  smallest possible JSON marshalling (`MarshalJSON`) with arrays to
  minimize size and parsing times. True and false are marshalled to 0
  and 1 as well. When printed (`Stringer`) however, a much more
  understandable form is used. Dependency on JSON tags that required
  slower reflection has been alleviated by implemented the JSON
  interfaces instead.

* Tokens of type `string` instead of `[]byte` or even `rune`
  because of convenience. The entire package is `string`-centric.

* Forced passing initial Value of Node when created to prompt
  developers not to forget it. Having the literal Value is essential
  to recreating the entire data stream later and to providing
  valuable error reporting information.
