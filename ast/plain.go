package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// Plain <-- Primary Quant?
func Plain(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Plain, nd.Types)

	var err error
	var n *pegn.Node

	// Primary
	n, err = Primary(p)
	if err != nil {
		return expected("Primary", node, p)
	}
	node.AdoptFrom(n)

	// Quant?
	n, err = Quant(p)
	if err == nil {
		node.AdoptFrom(n)
	}

	return node, nil
}
