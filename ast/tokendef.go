package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// TokenDef <-- (ResTokenId / TokenId) SP+ '<-' SP+
//              (Rune / SQ String SQ / ResTokenId / TokenId)
//              ComEndLine
func TokenDef(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.TokenDef, nd.Types)

	var err error
	var n *pegn.Node

	// (ResTokenId / TokenId)
	for {

		// ResTokenId
		n, err = ResTokenId(p)
		if err == nil {
			break
		}

		// TokenId
		n, err = TokenId(p)
		if err == nil {
			break
		}

		return expected("(ResTokenId / TokenId)", node, p)
	}
	node.AppendChild(n)

	// SP+ '<-' SP+
	if p.Expect(is.Min{' ', 1}, "<-", is.Min{' ', 1}) == nil {
		return expected("SP+ '<-' SP+", node, p)
	}

	for {

		// Rune
		n, err = Rune(p)
		if err == nil {
			node.AdoptFrom(n)
			break
		}

		for {

			b := p.Mark()

			// SQ
			if p.Expect('\'') == nil {
				p.Goto(b)
				break
			}

			// String
			n, err = String(p)
			if err != nil {
				p.Goto(b)
				break
			}

			// SQ
			if p.Expect('\'') == nil {
				p.Goto(b)
				break
			}

			node.AdoptFrom(n)

		}

		// ResTokenId
		n, err = ResTokenId(p)
		if err == nil {
			node.AppendChild(n)
			break
		}

		// TokenId
		n, err = TokenId(p)
		if err == nil {
			node.AppendChild(n)
			break
		}

		return expected("TokenId", node, p)
	}

	// ComEndLine
	n, err = ComEndLine(p)
	if err != nil {
		return expected("ComEndLine", node, p)
	}
	node.AdoptFrom(n)

	return node, nil
}
