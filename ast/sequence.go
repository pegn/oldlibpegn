package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// Sequence <-- Rule (Spacing Rule)*
func Sequence(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Sequence, nd.Types)

	var err error
	var n *pegn.Node

	// Rule
	n, err = Rule(p)
	if err != nil {
		return expected("Rule", node, p)
	}
	node.AdoptFrom(n)

	// (Spacing Rule)*
	for {

		beg := p.Mark()

		// Spacing
		sp, err := Spacing(p)
		if err != nil {
			p.Goto(beg)
			break
		}

		// Rule
		rl, err := Rule(p)
		if err != nil {
			p.Goto(beg)
			break
		}

		node.AdoptFrom(sp)
		node.AdoptFrom(rl)

	}

	return node, nil
}
