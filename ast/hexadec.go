package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// Hexadec <-- 'x' upperhex+
func Hexadec(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Hexadec, nd.Types)

	var m *pegn.Mark

	// 'x' upperhex+
	m = p.Check("x", is.Min{is.UpperHex, 1})
	if m == nil {
		return expected("'x' upperhex+", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
