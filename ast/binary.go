package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// Binary <-- "b" bitdig+
func Binary(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Binary, nd.Types)

	var m *pegn.Mark

	// "b" bin+
	m = p.Check("b", is.Min{is.Bitdig, 1})
	if m == nil {
		return expected("'b' [0-1]+", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
