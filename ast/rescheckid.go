package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// ResCheckId <-- 'EndLine'
func ResCheckId(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.ResCheckId, nd.Types)

	var m *pegn.Mark

	// 'EndLine'
	m = p.Check("EndLine")
	if m == nil {
		return expected("'EndLine'", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
