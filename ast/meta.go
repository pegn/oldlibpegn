package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// Meta <- '# ' Language ' (' Version ') ' Home EndLine
func Meta(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Meta, nd.Types)

	var err error
	var n *pegn.Node

	// '# '
	if p.Expect("# ") == nil {
		return expected("'# '", node, p)
	}

	// Language
	n, err = Language(p)
	if err != nil {
		return expected("Language", node, p)
	}
	node.AdoptFrom(n)

	// ' ('
	if p.Expect(" (") == nil {
		return expected("' ('", node, p)
	}

	// Version
	n, err = Version(p)
	if err != nil {
		return expected("Version", node, p)
	}
	node.AdoptFrom(n)

	// ') '
	if p.Expect(") ") == nil {
		return expected("') '", node, p)
	}

	// Home
	n, err = Home(p)
	if err != nil {
		return expected("Home", node, p)
	}
	node.AppendChild(n)

	// EndLine
	n, err = EndLine(p)
	if err != nil {
		return expected("EndLine", node, p)
	}
	node.AppendChild(n)

	return node, nil
}
