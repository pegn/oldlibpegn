package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// Primary <- Simple / CheckId / '(' Expression ')'
func Primary(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Primary, nd.Types)

	var err error
	var n *pegn.Node

	// Primary
	n, err = Simple(p)
	if err == nil {
		node.AdoptFrom(n)
		return node, nil
	}

	// CheckId
	n, err = CheckId(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}
	b := p.Mark()

	// '('
	if p.Expect('(') == nil {
		p.Goto(b)
		return expected("'('", node, p)
	}

	// Expression
	n, err = Expression(p)
	if err != nil {
		p.Goto(b)
		return expected("Expression", node, p)
	}

	// ')'
	if p.Expect(')') == nil {
		p.Goto(b)
		return expected("')'", node, p)
	}

	node.AppendChild(n)

	return node, nil
}
