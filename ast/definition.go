package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// Definition <- SchemaDef / CheckDef / ClassDef / TokenDef
func Definition(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Definition, nd.Types)

	var err error
	var n *pegn.Node

	// SchemaDef
	n, err = SchemaDef(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}
	// CheckDef
	n, err = CheckDef(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// ClassDef
	n, err = ClassDef(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// TokenDef
	n, err = TokenDef(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	return expected("SchemaDef / CheckDef / ClassDef / TokenDef", node, p)
}
