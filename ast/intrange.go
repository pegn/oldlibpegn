package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// IntRange <-- '[' Integer '-' Integer ']'
func IntRange(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.IntRange, nd.Types)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// '['
	if p.Expect('[') == nil {
		p.Goto(beg)
		return expected("'['", node, p)
	}

	// Integer
	n, err = Integer(p)
	if err != nil {
		p.Goto(beg)
		return expected("Integer", node, p)
	}
	node.AppendChild(n)

	// '-'
	if p.Expect('-') == nil {
		p.Goto(beg)
		return expected("'-'", node, p)
	}

	// Integer
	n, err = Integer(p)
	if err != nil {
		p.Goto(beg)
		return expected("Integer", node, p)
	}
	node.AppendChild(n)

	// ']'
	if p.Expect(']') == nil {
		p.Goto(beg)
		return expected("']'", node, p)
	}

	return node, nil
}
