package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// ResClassId <-- 'alphanum' / 'alpha' / 'any' / 'bitdig' / 'control'
//              / 'digit' / 'hexdig' / 'lowerhex' / 'lower' / 'octdig'
//              / 'punct' / 'quotable' / 'sign' / 'upperhex' / 'upper'
//              / 'visible' / 'ws' / 'alnum' / 'ascii' / 'blank' / 'cntrl'
//              / 'graph' / 'print' / 'space' / 'word' / 'xdigit'
func ResClassId(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.ResClassId, nd.Types)
	var m *pegn.Mark

	for _, v := range []string{
		"alphanum", "alpha", "any", "bitdig", "control", "digit", "hexdig",
		"lowerhex", "lower", "octdig", "punct", "quotable", "sign",
		"upperhex", "upper", "visible", "ws", "alnum", "ascii", "blank",
		"cntrl", "graph", "print", "space", "word", "xdigit",
	} {
		m = p.Check(v)
		if m == nil {
			continue
		}
		node.Value += p.Parse(m)
		p.Goto(m)
		p.Next()

		return node, nil
	}

	return expected("'alphanum' / 'alpha' / 'any' / 'bitdig' / 'control' / 'digit' / 'hexdig' / 'lowerhex' / 'lower' / 'octdig' / 'punct' / 'quotable' / 'sign' / 'upperhex' / 'upper' / 'visible' / 'ws' / 'alnum' / 'ascii' / 'blank' / 'cntrl' / 'graph' / 'print' / 'space' / 'word' / 'xdigit'", node, p)
}
