package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// Expression <-- Sequence (Spacing '/' SP+ Sequence)*
func Expression(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Expression, nd.Types)

	var err error
	var n *pegn.Node

	// Sequence
	n, err = Sequence(p)
	if err != nil {
		return expected("Sequence", node, p)
	}
	node.AppendChild(n)

	for {

		b := p.Mark()

		// Spacing
		sp, err := Spacing(p)
		if err != nil {
			p.Goto(b)
			break
		}

		// '/' SP+
		if p.Expect('/', is.Min{' ', 1}) == nil {
			p.Goto(b)
			break
		}

		// Sequence
		sq, err := Sequence(p)
		if err != nil {
			p.Goto(b)
			break
		}

		node.AdoptFrom(sp)
		node.AppendChild(sq)

	}

	return node, nil
}
