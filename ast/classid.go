package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// ClassId <-- ResClassId / lower (lower / UNDER lower)+
func ClassId(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.ClassId, nd.Types)

	var m *pegn.Mark
	var n *pegn.Node
	var err error

	// ResClassId
	n, err = ResClassId(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// lower (lower / UNDER lower)+
	m = p.Check(is.Lower, is.Min{is.OneOf{is.Lower, is.Seq{'_', is.Lower}}, 0})
	if m == nil {
		return expected("lower (lower / UNDER lower)+", node, p)
	}
	node.Value = p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
