package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// Licensed <-- '# Licensed under ' Comment EndLine
func Licensed(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Licensed, nd.Types)

	var err error
	var n *pegn.Node
	b := p.Mark()

	// '# Licensed under '
	if p.Expect("# Licensed under ") == nil {
		p.Goto(b)
		return expected("'# Licensed under '", node, p)
	}

	// Comment
	n, err = Comment(p)
	if err != nil {
		p.Goto(b)
		return expected("Comment", node, p)
	}
	node.AppendChild(n)

	// EndLine
	n, err = EndLine(p)
	if err != nil {
		p.Goto(b)
		return expected("EndLine", node, p)
	}
	node.AppendChild(n)

	return node, nil
}
