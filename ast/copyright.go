package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// Copyright <-- '# Copyright ' Comment EndLine
func Copyright(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Copyright, nd.Types)

	var err error
	var n *pegn.Node
	b := p.Mark()

	// '# Copyright '
	if p.Expect("# Copyright ") == nil {
		p.Goto(b)
		return expected("'# Copyright '", node, p)
	}

	// Comment
	n, err = Comment(p)
	if err != nil {
		p.Goto(b)
		return expected("Comment", node, p)
	}
	node.AppendChild(n)

	// EndLine
	n, err = EndLine(p)
	if err != nil {
		p.Goto(b)
		return expected("EndLine", node, p)
	}
	node.AppendChild(n)

	return node, nil
}
