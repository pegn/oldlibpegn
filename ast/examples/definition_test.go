package ast_test

import (
	"fmt"

	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleDefinition_schema() {
	var n *pegn.Node
	p := parser.New()
	p.Init("Blah <-- Some Thing*")
	n, _ = ast.Definition(p)
	n.FirstChild.Print()
	// Output:
	// ["SchemaDef", [
	//   ["CheckId", "Blah"],
	//   ["Expression", [
	//     ["Sequence", [
	//       ["Plain", [
	//         ["CheckId", "Some"]
	//       ]],
	//       ["Plain", [
	//         ["CheckId", "Thing"],
	//         ["MinZero", "*"]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}

func ExampleDefinition_check() {
	var n *pegn.Node
	var err error
	p := parser.New()
	p.Init("Blah <- Some Thing*")
	n, err = ast.Definition(p)
	if err != nil {
		fmt.Println(err)
	} else {
		n.FirstChild.Print()
	}
	// Output:
	// ["CheckDef", [
	//   ["CheckId", "Blah"],
	//   ["Expression", [
	//     ["Sequence", [
	//       ["Plain", [
	//         ["CheckId", "Some"]
	//       ]],
	//       ["Plain", [
	//         ["CheckId", "Thing"],
	//         ["MinZero", "*"]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}

func ExampleDefinition_class() {
	var n *pegn.Node
	p := parser.New()
	p.Init("my_class <- Some Thing*") // illegal
	n, _ = ast.Definition(p)
	n.Print()
	p.Init("my_class <- [a-Z] / [0-9] / UNDER")
	n, _ = ast.Definition(p)
	n.Print()
	// Output:
	// <nil>
	// ["Definition", [
	//   ["ClassDef", [
	//     ["ClassId", "my_class"],
	//     ["ClassExpr", [
	//       ["AlphaRange", [
	//         ["Alpha", "a"],
	//         ["Alpha", "Z"]
	//       ]],
	//       ["IntRange", [
	//         ["Integer", "0"],
	//         ["Integer", "9"]
	//       ]],
	//       ["TokenId", [
	//         ["ResTokenId", "UNDER"]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}
