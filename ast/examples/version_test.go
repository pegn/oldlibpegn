package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleVersion() {

	var n *pegn.Node

	// Version <- 'v' MajorVer '.' MinorVer '.' PatchVer
	p := parser.New()

	// v0.31.1
	p.Init("v0.31.1")
	n, _ = ast.Version(p)
	n.Print()

	// Output:
	// ["Version", [
	//   ["MajorVer", "0"],
	//   ["MinorVer", "31"],
	//   ["PatchVer", "1"]
	// ]]

}
