package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleSpacing() {
	var n *pegn.Node

	// Spacing <- ComEndLine? SP+
	p := parser.New()

	p.Init(" ")
	n, _ = ast.Spacing(p)
	n.Print()

	p.Init("   # Comment\n ")
	n, _ = ast.Spacing(p)
	n.Print()

	// Output:
	// ["Spacing", ""]
	// ["Spacing", [
	//   ["Comment", "Comment"],
	//   ["EndLine", "\n"]
	// ]]

}
