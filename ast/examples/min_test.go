package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleMin() {

	var n *pegn.Node

	// Min <-- digit+
	p := parser.New()

	// 1
	p.Init("1")
	n, _ = ast.Min(p)
	n.Print()

	// 99
	p.Init("99P")
	n, _ = ast.Min(p)
	n.Print()

	p.Print()

	// Output:
	// ["Min", "1"]
	// ["Min", "99"]
	// U+0050 'P' 1,3-3 (3-3)

}
