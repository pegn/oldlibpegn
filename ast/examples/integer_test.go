package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleInteger() {

	var n *pegn.Node
	p := parser.New()

	// 0
	p.Init("0")
	n, _ = ast.Integer(p)
	n.Print()

	// 99
	p.Init("99")
	n, _ = ast.Integer(p)
	n.Print()

	// Output:
	// ["Integer", "0"]
	// ["Integer", "99"]

}
