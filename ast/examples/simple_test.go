package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleSimple_rune() {
	var n *pegn.Node
	p := parser.New()
	p.Init("U+FFFD")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["Unicode", "U+FFFD"]
	// ]]
}

func ExampleSimple_hexadec() {
	var n *pegn.Node
	p := parser.New()
	p.Init("x64")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["Hexadec", "x64"]
	// ]]
}

func ExampleSimple_binary() {
	var n *pegn.Node
	p := parser.New()
	p.Init("b101")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["Binary", "b101"]
	// ]]
}

func ExampleSimple_octal() {
	var n *pegn.Node
	p := parser.New()
	p.Init("o774")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["Octal", "o774"]
	// ]]
}

func ExampleSimple_resclassid() {
	var n *pegn.Node
	p := parser.New()
	p.Init("any")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["ClassId", [
	//     ["ResClassId", "any"]
	//   ]]
	// ]]
}

func ExampleSimple_classid() {
	var n *pegn.Node
	p := parser.New()
	p.Init("my_class")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["ClassId", "my_class"]
	// ]]
}

func ExampleSimple_restokenid() {
	var n *pegn.Node
	p := parser.New()
	p.Init("DOT")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["TokenId", [
	//     ["ResTokenId", "DOT"]
	//   ]]
	// ]]
}

func ExampleSimple_tokenid() {
	var n *pegn.Node
	p := parser.New()
	p.Init("ALL_MINE")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["TokenId", "ALL_MINE"]
	// ]]
}

func ExampleSimple_unirange() {
	var n *pegn.Node
	p := parser.New()
	p.Init("[U+0020-U+0025]")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["UniRange", [
	//     ["Unicode", "U+0020"],
	//     ["Unicode", "U+0025"]
	//   ]]
	// ]]
}

func ExampleSimple_alpharange() {
	var n *pegn.Node
	p := parser.New()
	p.Init("[a-m]")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["AlphaRange", [
	//     ["Alpha", "a"],
	//     ["Alpha", "m"]
	//   ]]
	// ]]
}

func ExampleSimple_intrange() {
	var n *pegn.Node
	p := parser.New()
	p.Init("[0-33]")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["IntRange", [
	//     ["Integer", "0"],
	//     ["Integer", "33"]
	//   ]]
	// ]]
}

func ExampleSimple_binrange() {
	var n *pegn.Node
	p := parser.New()
	p.Init("[b10-b10110]")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["BinRange", [
	//     ["Binary", "b10"],
	//     ["Binary", "b10110"]
	//   ]]
	// ]]
}

func ExampleSimple_hexrange() {
	var n *pegn.Node
	p := parser.New()
	p.Init("[x20-x05A]")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["HexRange", [
	//     ["Hexadec", "x20"],
	//     ["Hexadec", "x05A"]
	//   ]]
	// ]]
}

func ExampleSimple_string() {
	var n *pegn.Node
	p := parser.New()
	p.Init("'some string'")
	n, _ = ast.Simple(p)
	n.Print()
	// Output:
	// ["Simple", [
	//   ["String", "some string"]
	// ]]
}
