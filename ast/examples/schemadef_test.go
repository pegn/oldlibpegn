package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleSchemaDef() {
	var n *pegn.Node
	p := parser.New()
	p.Init("Schema <-- SP")
	n, _ = ast.SchemaDef(p)
	n.Print()
	// Output:
	// ["SchemaDef", [
	//   ["CheckId", "Schema"],
	//   ["Expression", [
	//     ["Sequence", [
	//       ["Plain", [
	//         ["TokenId", [
	//           ["ResTokenId", "SP"]
	//         ]]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}
