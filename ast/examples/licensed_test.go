package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleLicensed() {
	var n *pegn.Node
	p := parser.New()
	p.Init("# Licensed under Apache-2, CC-BY-4.0\n")
	n, _ = ast.Licensed(p)
	n.Print()
	// Output:
	// ["Licensed", [
	//   ["Comment", "Apache-2, CC-BY-4.0"],
	//   ["EndLine", "\n"]
	// ]]
}
