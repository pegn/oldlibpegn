package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleMinZero() {
	var n *pegn.Node

	// MinZero <-- "*"
	p := parser.New()

	// *
	p.Init("*")
	n, _ = ast.MinZero(p)
	n.Print()

	// Output:
	// ["MinZero", "*"]

}
