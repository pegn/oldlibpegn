package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleAlpha() {

	var n *pegn.Node

	// Alpha <-- alpha
	p := parser.New()

	// a
	p.Init("a")
	n, _ = ast.Alpha(p)
	n.Print()

	// A
	p.Init("A")
	n, _ = ast.Alpha(p)
	n.Print()

	// z
	p.Init("z")
	n, _ = ast.Alpha(p)
	n.Print()

	// Z
	p.Init("Z")
	n, _ = ast.Alpha(p)
	n.Print()

	// _
	p.Init("_")
	n, _ = ast.Alpha(p)
	n.Print()

	// Output:
	// ["Alpha", "a"]
	// ["Alpha", "A"]
	// ["Alpha", "z"]
	// ["Alpha", "Z"]
	// <nil>

}
