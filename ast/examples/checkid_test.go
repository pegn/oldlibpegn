package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleCheckId() {
	var n *pegn.Node

	// CheckId <-- ResCheckId / (upper lower+)+
	p := parser.New()

	p.Init("EndLine")
	n, _ = ast.CheckId(p)
	n.Print()

	p.Init("CheckId")
	n, _ = ast.CheckId(p)
	n.Print()

	p.Init("AnotherId")
	n, _ = ast.CheckId(p)
	n.Print()

	// invalid
	p.Init("invalid")
	n, _ = ast.CheckId(p)
	n.Print()

	// Output:
	// ["CheckId", [
	//   ["ResCheckId", "EndLine"]
	// ]]
	// ["CheckId", "CheckId"]
	// ["CheckId", "AnotherId"]
	// <nil>

}
