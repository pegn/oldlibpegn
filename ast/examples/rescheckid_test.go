package ast_test

import (
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleResCheckId() {

	p := parser.New()
	p.Init("EndLine")
	node, _ := ast.ResCheckId(p)
	node.Print()

	// Output:
	// ["ResCheckId", "EndLine"]
}
