package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleCopyright() {
	var n *pegn.Node
	p := parser.New()
	p.Init("# Copyright 2020 Robert S Muhlestein (rwx@robs.io)\n")
	n, _ = ast.Copyright(p)
	n.Print()
	// Output:
	// ["Copyright", [
	//   ["Comment", "2020 Robert S Muhlestein (rwx@robs.io)"],
	//   ["EndLine", "\n"]
	// ]]
}
