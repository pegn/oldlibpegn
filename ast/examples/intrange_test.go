package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleIntRange() {
	var n *pegn.Node

	p := parser.New()

	// [0-9]
	p.Init("[0-9]")
	n, _ = ast.IntRange(p)
	n.Print()

	// [00-99]
	p.Init("[00-99]")
	n, _ = ast.IntRange(p)
	n.Print()

	// Output:
	// ["IntRange", [
	//   ["Integer", "0"],
	//   ["Integer", "9"]
	// ]]
	// ["IntRange", [
	//   ["Integer", "00"],
	//   ["Integer", "99"]
	// ]]

}
