package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExamplePosLook_plain() {
	var n *pegn.Node
	p := parser.New()
	p.Init("&MyCheck")
	n, _ = ast.PosLook(p)
	n.Print()
	// Output:
	// ["PosLook", [
	//   ["CheckId", "MyCheck"]
	// ]]
}

func ExamplePosLook_minzero() {
	var n *pegn.Node
	p := parser.New()
	p.Init("&MyZeroOrMore*")
	n, _ = ast.PosLook(p)
	n.Print()
	// Output:
	// ["PosLook", [
	//   ["CheckId", "MyZeroOrMore"],
	//   ["MinZero", "*"]
	// ]]
}

func ExamplePosLook_minone() {
	var n *pegn.Node
	p := parser.New()
	p.Init("&MyOneOrMore+")
	n, _ = ast.PosLook(p)
	n.Print()
	// Output:
	// ["PosLook", [
	//   ["CheckId", "MyOneOrMore"],
	//   ["MinOne", "+"]
	// ]]
}

func ExamplePosLook_minmax() {
	var n *pegn.Node
	p := parser.New()
	p.Init("&MyMinMax{2,4}")
	n, _ = ast.PosLook(p)
	n.Print()
	// Output:
	// ["PosLook", [
	//   ["CheckId", "MyMinMax"],
	//   ["MinMax", [
	//     ["Min", "2"],
	//     ["Max", "4"]
	//   ]]
	// ]]
}
