package ast_test

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast"
	"gitlab.com/pegn/libpegn/parser"
)

func ExampleRange() {
	var n *pegn.Node

	// Range <- AlphaRange / IntRange / UniRange
	//        / BinRange / HexRange / OctRange
	p := parser.New()

	// [U+0000-U+00FF]
	p.Init("[U+0000-U+00FF]")
	n, _ = ast.UniRange(p)
	n.Print()

	// [a-z]
	p.Init("[a-z]")
	n, _ = ast.AlphaRange(p)
	n.Print()

	// [00-99]
	p.Init("[00-99]")
	n, _ = ast.IntRange(p)
	n.Print()

	// [b01-b10]
	p.Init("[b01-b10]")
	n, _ = ast.BinRange(p)
	n.Print()

	// [x0-xF]
	p.Init("[x0-xF]")
	n, _ = ast.HexRange(p)
	n.Print()

	// Output:
	// ["UniRange", [
	//   ["Unicode", "U+0000"],
	//   ["Unicode", "U+00FF"]
	// ]]
	// ["AlphaRange", [
	//   ["Alpha", "a"],
	//   ["Alpha", "z"]
	// ]]
	// ["IntRange", [
	//   ["Integer", "00"],
	//   ["Integer", "99"]
	// ]]
	// ["BinRange", [
	//   ["Binary", "b01"],
	//   ["Binary", "b10"]
	// ]]
	// ["HexRange", [
	//   ["Hexadec", "x0"],
	//   ["Hexadec", "xF"]
	// ]]

}
