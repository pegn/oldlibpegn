package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
	"gitlab.com/pegn/libpegn/tk"
)

// PreVer <-- (word / DASH)+ ('.' (word / DASH)+)*
func PreVer(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.PreVer, nd.Types)

	var m *pegn.Mark

	// (word / DASH)+
	m = p.Check(is.Min{is.OneOf{is.Word, tk.DASH}, 1})
	if m == nil {
		return expected("(word / DASH)+", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	// ('.' (word / DASH)+)*
	for {

		b := p.Mark()
		var m *pegn.Mark

		// PreId
		m = p.Check('.', is.Min{is.OneOf{is.Word, tk.DASH}, 1})
		if m == nil {
			p.Goto(b)
			break
		}
		node.Value += p.Parse(m)
		p.Goto(m)
		p.Next()

	}

	return node, nil
}
