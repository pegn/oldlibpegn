package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
	"gitlab.com/pegn/libpegn/is"
)

// Octal <-- 'o' octdig+
func Octal(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Octal, nd.Types)

	var m *pegn.Mark

	// 'o' oct+
	m = p.Check("o", is.Min{is.Octdig, 1})
	if m == nil {
		return expected("'o'", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
