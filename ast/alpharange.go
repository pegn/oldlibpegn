package ast

import (
	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/ast/nd"
)

// AlphaRange <-- '[' Alpha '-' Alpha ']'
func AlphaRange(p pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.AlphaRange, nd.Types)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// '['
	if p.Expect('[') == nil {
		p.Goto(beg)
		return expected("'['", node, p)
	}

	// Alpha
	n, err = Alpha(p)
	if n == nil {
		p.Goto(beg)
		return expected("alpha", node, p)
	}
	node.AppendChild(n)

	// '-'
	if p.Expect('-') == nil {
		p.Goto(beg)
		return expected("'-'", node, p)
	}

	// Alpha
	n, err = Alpha(p)
	if err != nil {
		p.Goto(beg)
		return expected("alpha", node, p)
	}
	node.AppendChild(n)

	// ']'
	if p.Expect(']') == nil {
		p.Goto(beg)
		return expected("']'", node, p)
	}

	return node, nil
}
