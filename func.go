package pegn

// CheckFunc if a first-class function that may be passed to the Check()
// and Expect() methods. It takes a parser as its only argument and uses
// it to look ahead some amount for a specific match as determined by
// the author.  If a match is found a Mark for the last rune in the
// match is returned such that the caller can p.Parse(end) reliably to
// return the matching string.
type CheckFunc func(p Parser) (*Mark, error)

// IsFunc is a first-class function type that may be passed to the
// Check() and Expect() methods. Simple IsFunc function implementations
// are provided in the 'is' subpackage but any compatible function from
// the unicode package will work as well
type IsFunc func(r rune) bool
