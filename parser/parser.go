package parser

import (
	"fmt"
	"io"
	"io/ioutil"
	"unicode/utf8"

	pegn "gitlab.com/pegn/libpegn"
	"gitlab.com/pegn/libpegn/is"
)

// New returns an instance of the default pegn.Parser implementation
// and is used by the functions from the ast and pf packages. Must call
// parser.Init() before using.
func New() pegn.Parser {
	n := new(parser)
	return n
}

type parser struct {
	in  io.Reader
	buf []byte
	mk  *pegn.Mark
}

func (p *parser) bufferInput(i interface{}) error {
	var err error
	switch in := i.(type) {
	case io.Reader:
		p.buf, err = ioutil.ReadAll(in)
		if err != nil {
			return err
		}
	case string:
		p.buf = []byte(in)
	case []byte:
		p.buf = in
	default:
		return fmt.Errorf("parser: unsupported input type: %t", i)
	}
	if len(p.buf) == 0 {
		return fmt.Errorf("parser: no input")
	}
	return err
}

func (p *parser) Peek() rune {
	rn, _ := utf8.DecodeRune(p.buf) // scan first
	return rn
}

func (p *parser) Init(i interface{}) error {
	if err := p.bufferInput(i); err != nil {
		return err
	}
	cur, ln := utf8.DecodeRune(p.buf) // scan first
	if ln == 0 {
		return fmt.Errorf("parser: failed to scan first rune")
	}
	p.mk = new(pegn.Mark)
	p.mk.Rune = cur
	p.mk.Len = ln
	p.mk.Next = ln
	p.mk.Pos.Line = 1
	p.mk.Pos.LineRune = 1
	p.mk.Pos.LineByte = 1
	p.mk.Pos.Rune = 1
	return nil
}

func (p *parser) Next() {
	if p.Done() {
		return
	}
	rn, ln := utf8.DecodeRune(p.buf[p.mk.Next:])
	if ln != 0 {
		p.mk.Byte = p.mk.Next
		p.mk.Pos.LineByte += p.mk.Len
	}
	p.mk.Rune = rn
	p.mk.Pos.Rune += 1
	p.mk.Next += ln
	p.mk.Pos.LineRune += 1
	p.mk.Len = ln
}

func (p *parser) Move(n int) {
	for i := 0; i < n; i++ {
		p.Next()
	}
}

func (p *parser) Done() bool {
	return p.mk.Rune == utf8.RuneError && p.mk.Len == 0
}

func (p *parser) String() string { return p.mk.String() }
func (p *parser) Print()         { fmt.Println(p) }

func (p *parser) Mark() *pegn.Mark {
	if p.mk == nil {
		return nil
	}
	// force a copy
	cp := *p.mk
	return &cp
}

func (p *parser) Goto(m *pegn.Mark) { nm := *m; p.mk = &nm }

func (p *parser) NewLine() {
	p.mk.Pos.Line++
	p.mk.Pos.LineRune = 1
	p.mk.Pos.LineByte = 1
}

func (p *parser) Parse(m *pegn.Mark) string {
	if m.Byte < p.mk.Byte {
		return string(p.buf[m.Byte:p.mk.Next])
	}
	return string(p.buf[p.mk.Byte:m.Next])
}

func (p *parser) Slice(beg *pegn.Mark, end *pegn.Mark) string {
	return string(p.buf[beg.Byte:end.Next])
}

func (p *parser) expected(a string) (*pegn.Node, error) {
	return nil, fmt.Errorf(`expected (%v) at %v`,
		a, p.Mark())
}

func (p *parser) Expect(ms ...interface{}) *pegn.Mark {
	var beg, end *pegn.Mark
	beg = p.Mark()
	for _, m := range ms {
		switch v := m.(type) {

		case rune:
			if p.mk.Rune != v {
				p.Goto(beg)
				return nil
			}
			end = p.Mark()
			p.Next()

		case string:
			for _, r := range []rune(v) {
				if p.mk.Rune != r {
					p.Goto(beg)
					return nil
				}
				end = p.Mark()
				p.Next()
			}

		case pegn.IsFunc, func(r int32) bool:
			if !v.(func(r int32) bool)(p.mk.Rune) {
				p.Goto(beg)
				return nil
			}
			end = p.Mark()
			p.Next()

		case pegn.CheckFunc, func(p pegn.Parser) *pegn.Mark:
			rv := v.(func(p pegn.Parser) *pegn.Mark)(p)
			if rv == nil {
				p.Goto(beg)
				return nil
			}
			end = rv
			p.Goto(rv)
			p.Next()

		case is.Not:
			if m := p.Check(v.This); m != nil {
				p.Goto(beg)
				return nil
			}
			end = p.Mark()

		case is.Min:
			c := 0
			last := p.Mark()
			for {
				m := p.Expect(v.Match)
				if m == nil {
					break
				}
				last = m
				c++
			}
			if c < v.Min {
				p.Goto(beg)
				return nil
			}
			end = last

		case is.Count:
			m := p.Expect(is.MinMax{v.Match, v.Count, v.Count})
			if m == nil {
				p.Goto(beg)
				return nil
			}
			end = m

		case is.MinMax:
			c := 0
			last := p.Mark()
			for {
				m := p.Expect(v.Match)
				if m == nil {
					break
				}
				last = m
				c++
			}
			if c == 0 && v.Min == 0 {
				if end == nil {
					end = last
				}
				continue
			}
			if !(v.Min <= c && c <= v.Max) {
				p.Goto(last)
				return nil
			}
			end = last

		case is.Seq:
			m := p.Expect(v...)
			if m == nil {
				p.Goto(beg)
				return nil
			}
			end = m

		case is.OneOf:
			var m *pegn.Mark
			for _, i := range v {
				m = p.Expect(i)
				if m != nil {
					break
				}
			}
			if m == nil {
				p.Goto(beg)
				return nil
			}
			end = m

		default:
			panic("invalid Check()/Expect() type")
		}
	}
	return end
}

// Check fulfills the Parser interface.
func (p *parser) Check(ms ...interface{}) *pegn.Mark {
	defer p.Goto(p.Mark())
	return p.Expect(ms...)
}
